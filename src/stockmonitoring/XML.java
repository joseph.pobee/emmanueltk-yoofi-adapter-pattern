package stockmonitoring;

import java.util.List;

interface XML {
    public List<String> getHeaders();

    public String getKeyData(String key);
}
