package stockmonitoring;

import java.util.*;

class Node {
    String key;
    Integer value;

    public Node(String key, Integer value) {
        this.key = key;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Node{" +
                "key='" + key + '\'' +
                ", value=" + value +
                '}';
    }
}

public class NewAnalyticsChartMaker {
    public void displayChart(JSON data) {
        List<String> keys = data.getKeys();
        Map<String, String> sampleData = (Map<String, String>) data.getValue(keys.get(0));
        Set<String> dataParams = sampleData.keySet();
        Map<String, List<Node>> dataPoints = new HashMap<>();
        for(String key : keys) {
            for(String param : dataParams) {
                Map<String, String> keyData = (Map<String, String>) data.getValue(key);
                if(!dataPoints.containsKey(param)) {
                    List<Node> newList = new ArrayList<>();
                    newList.add(new Node(key, Integer.valueOf(keyData.get(param))));
                    dataPoints.put(param, newList);
                } else {
                    List<Node> newList = dataPoints.get(param);
                    newList.add(new Node(key, Integer.valueOf(keyData.get(param))));
                    dataPoints.put(param, newList);
                }
            }
        }
        displayData(dataPoints);
    }

    private void displayData(Map<String, List<Node>> dataPoints) {
        for(Map.Entry<String, List<Node>> entry : dataPoints.entrySet()) {
            System.out.println(entry.getKey() + "\n");
            for(Node n : entry.getValue()) {
                String sb = padRight(n.key, 4) + ":" +
                        "*".repeat(Math.max(0, n.value));
                System.out.println(sb);
            }
        }
    }

    public static String padRight(String s, int n) {
        return String.format("%-" + n + "s", s);
    }

}
