package stockmonitoring;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JSONImplementation implements JSON {
    private final Map<String, Object> dataSet;

    public JSONImplementation(Map<String, Object> data){
        dataSet = data;
    }

    @Override
    public List<String> getKeys() {
        return dataSet.keySet().stream().toList();
    }

    @Override
    public Object getValue(String key) {
        return dataSet.get(key);
    }
}
