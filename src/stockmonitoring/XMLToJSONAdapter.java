package stockmonitoring;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class XMLToJSONAdapter implements JSON {
    XML xml;

    public XMLToJSONAdapter(XML xml) {
        this.xml = xml;
    }


    private Map<String, Map<String, String>> convertXMLToJSON(XML data) {
        Map<String, Map<String, String>> mapData = new HashMap<>();
        List<String> headers = data.getHeaders();
        for(String header : headers) {
            mapData.put(header, convertToJSONValue(header));
        }
        return mapData;
    }

    private Map<String, String> convertToJSONValue(String key) {
        Map<String, String> headerSubData = new HashMap<>();
        String value = xml.getKeyData(key);
        String[] dataset = value.split(",");
        for(String keyValue: dataset) {
            String[] splitData = keyValue.split(":");
            headerSubData.put(splitData[0], splitData[1]);
        }
        return headerSubData;
    }

    @Override
    public List<String> getKeys() {
        return xml.getHeaders();
    }

    @Override
    public Object getValue(String key) {
        return convertXMLToJSON(xml).get(key);
    }

}
