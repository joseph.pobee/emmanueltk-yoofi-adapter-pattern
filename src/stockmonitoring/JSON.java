package stockmonitoring;

import java.util.List;

interface JSON {
    public List<String> getKeys();

    public Object getValue(String key);
}
