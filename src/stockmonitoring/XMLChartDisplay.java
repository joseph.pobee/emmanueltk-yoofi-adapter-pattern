package stockmonitoring;

import java.util.List;

class XMLChartDisplay {
    public void displayChart(XML data) {
        List<String> headers = data.getHeaders();
        for (String header : headers) {
            System.out.println(header);
            System.out.println(data.getKeyData(header) + "\n");
        }
    }

    public static void main(String[] args) {

    }
}
