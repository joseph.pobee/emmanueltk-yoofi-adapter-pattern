package stockmonitoring;

public class StockDataDisplayApp {
    public static void main(String[] args) {
        //Create instance of stock data provider
        StockDataProvider provider = new StockDataProvider();

        //obtain xml data from provider
        XML xmlData = provider.getStockData();

        //create instance of xml compatible chart display
        XMLChartDisplay xmlDisplay = new XMLChartDisplay();

        //pass xml data to display library to show data
        xmlDisplay.displayChart(xmlData);
    }
}

