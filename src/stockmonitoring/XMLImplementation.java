package stockmonitoring;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.Element;


public class XMLImplementation implements XML {
    private final Document data;

    public XMLImplementation(Document data) {
        this.data = data;
    }

    @Override
    public List<String> getHeaders() {
        List<String> headers = new ArrayList<>();
        Element root = data.getRootElement();
        for (Iterator<Element> it = root.elementIterator("stock"); it.hasNext(); ) {
            Element element = it.next();
            headers.add(element.attribute("ticker").getValue());
        }
        return headers;
    }

    @Override
    public String getKeyData(String key) {
        Element root = data.getRootElement();
        for (Iterator<Element> it = root.elementIterator("stock"); it.hasNext(); ) {
            Element element = it.next();
            if (key.equals(element.attribute("ticker").getValue())) {
                StringBuilder sb = new StringBuilder();
                for (Iterator<Element> subIt = element.elementIterator(); subIt.hasNext(); ) {
                    Element subElement = subIt.next();
                    sb.append(String.format("%s:%s", subElement.getName(), subElement.getText()));
                    if (subIt.hasNext()) sb.append(String.format(",%n"));
                }
                return sb.toString();
            }
        }
        return "";
    }
}
