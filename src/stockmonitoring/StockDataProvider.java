package stockmonitoring;

import org.dom4j.Document;
import org.dom4j.io.SAXReader;

class StockDataProvider {
    public XML getStockData() {
        try {
            SAXReader reader = new SAXReader();
            Document document = reader.read("src/stockmonitoring/data.xml");
            return new XMLImplementation(document);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
